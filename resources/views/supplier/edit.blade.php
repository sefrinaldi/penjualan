@extends('layouts.template')

@section('title')
    Edit Data Supplier
@endsection

@section('content')
<div class="container">
  <div class="row">
    <div class="col-md-12">
      <div class="panel panel-default card">
        <div class="panel-heading">
          @include('alert.error')                           
        </div>

        <div class="panel-body">
          <form class="form-horizontal" method="post" action="{{ route('supplier.update',[$supplier->kd_supplier]) }}">
          {{ csrf_field() }}
          {{ method_field('PUT') }}
            <div class="panel-body post-body">
              <div class="form-group ">
                <label for="nama_supplier" class="col-sm-2 control-label">Nama Supplier</label>
                <div class="col-sm-10">
                  <input type="text" class="form-control" id="nama_supplier" name="nama_supplier" value="{{ $supplier->nama_supplier }}"></input>
                </div>
              </div>

              <div class="form-group">
                <label for="alamat_supplier" class="col-sm-2 control-label">Alamat Supplier</label>
                <div class="col-sm-10">
                  <textarea class="form-control" id="alamat_supplier" name="alamat_supplier">{{ $supplier->alamat_supplier }}</textarea>
                </div>
              </div>              
            </div>

            <div class="panel-footer col-sm-10">    
              <a class="btn btn-info float-right" href="{{ route('supplier.index') }}">Back</a>
              <button type="submit" name="tombol" class="btn btn-info float-right">Update</button>          
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection