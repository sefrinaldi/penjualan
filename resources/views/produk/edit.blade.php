@extends('layouts.template')

@section('title')
    Update Data Produk
@endsection

@section('content')
<div class="container">
  <div class="row">
    <div class="col-md-12">
      <div class="panel panel-default card">
        <div class="panel-heading">
          @include('alert.error')                           
        </div>

        <div class="panel-body">
          <form class="form-horizontal" method="post" action="{{ route('produk.update', [$produk->kd_produk]) }}" enctype="multipart/form-data">
          {{ csrf_field() }}
          {{ method_field('PUT') }}
            <div class="panel-body post-body">
              <div class="form-group ">
                <label for="nama_produk" class="col-sm-2 control-label">Nama Produk</label>
                <div class="col-sm-10">
                  <input type="text" class="form-control" id="nama_produk" name="nama_produk" value="{{ $produk->nama_produk }}"></input>
                </div>
              </div>

              <div class="form-group">
                <label for="kd_kategori" class="col-sm-2 control-label">Kategori Produk</label>
                <div class="col-sm-10">
                  <select class="form-control" id="kd_kategori" name="kd_kategori">
                    @foreach($kategori as $row)
                      <option value="{{ $row->kd_kategori }}" @if($produk->kd_kategori == $row->kd_kategori) Selected @endif >{{ $row->kategori }}</option>
                    @endforeach
                  </select>                  
                </div>
              </div>  

              <div class="form-group">
                <label for="harga" class="col-sm-2">Harga</label>
                <div class="col-sm-10">
                  <input type="number" class="form-control" id="harga" name="harga" value="{{ $produk->harga }}"></input>
                </div>
              </div> 

              <div class="form-group">    
                <label for="gambar_produk" class="col-sm-2"></label>            
                <div class="col-sm-10">
                  <img class="img-thumbnail" src="{{ asset('uploads/'.$produk->gambar_produk) }}" width="100px" />
                </div>
              </div>  

              <div class="form-group">
                <label for="gambar_produk" class="col-sm-2">Gambar Produk</label>
                <div class="col-sm-10">
                  <input type="file" class="form-control" id="gambar_produk" name="gambar_produk"></input>
                </div>
              </div>                         

            <div class="panel-footer col-sm-10">    
              <a class="btn btn-info float-right" href="{{ route('produk.index') }}">Back</a>
              <button type="submit" name="tombol" class="btn btn-info float-right">Update</button>          
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection