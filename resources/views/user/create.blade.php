@extends('layouts.template')

@section('title')
    Tambah Data User
@endsection

@section('content')
<div class="container">
  <div class="row">
    <div class="col-md-12">
      <div class="panel panel-default card">
        <div class="panel-heading">
          @include('alert.error')                           
        </div>

        <div class="panel-body">
          <form class="form-horizontal" method="post" action="{{ route('user.store') }}">
          {{ csrf_field() }}
            <div class="panel-body post-body">
              <div class="form-group ">
                <label for="name" class="col-sm-2 control-label">Nama</label>
                <div class="col-sm-10">
                  <input type="text" class="form-control" id="name" name="name" value="{{ old('name') }}"></input>
                </div>
              </div>

              <div class="form-group">
                <label for="username" class="col-sm-2 control-label">Username</label>
                <div class="col-sm-10">
                  <input type="text" class="form-control" id="username" name="username" value="{{ old('username') }}"></input>
                </div>
              </div>

              <div class="form-group">
                <label for="email" class="col-sm-2 control-label">Email</label>
                <div class="col-sm-10">
                  <input type="email" class="form-control" id="email" name="email" value="{{ old('email') }}"></input>
                </div>
              </div>

              <div class="form-group">
                <label for="password" class="col-sm-2 control-label">Password</label>
                <div class="col-sm-10">
                  <input type="password" class="form-control" id="password" name="password" value=""></input>
                </div>
              </div>

              <div class="form-group">
                <label for="level" class="col-sm-10 control-label">Label</label>
                <div class="col-sm-10">
                  <select name="level" id="level" class="form-control">
                    <option value="admin">Administrator</option>
                    <option value="staff">Staff</option>
                  </select>
                </div>
              </div>
            </div>

            <div class="panel-footer col-sm-10">    
              <a class="btn btn-info float-right" href="{{ route('user.index') }}">Back</a>
              <button type="submit" name="tombol" class="btn btn-info float-right">Save</button>          
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection