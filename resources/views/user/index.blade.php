@extends('layouts.template')

@section('title')
    Data User
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
          <div class="panel panel-default">
            <div class="panel-heading">
                @if(Request::get('keyword'))
                    <a class="btn btn-success" href="{{ route('user.index') }}">Back</a>
                @else
                    <a class="btn btn-success" href="{{ route('user.create') }}"><span class="glyphicon glyphicon-plus"></span>Create</a>
                @endif                

                <form method="get" action="{{route('user.index')}}">
                    <div class="form-group">
                    <label for="keyword" class="col-sm-2 control-label">Search By Name</label>
                    <div class="col-sm-4">
                        <input type="text" class="form-control" id="keyword" name="keyword" value="{{Request::get('keyword')}}">
                    </div>
                    <div class="col-sm-6">
                        <button type="submit" class="btn btn-info"><span class="glyphicon glyphicon-search" aria-hidden="true"></span></button>
                    </div>
                    </div>
                </form>                            
            </div>
            <div class="panel panel-body">
                @if(Request::get('keyword'))
                    <div class="alert alert-success alert-block">
                        Hasil pencarian user dengan keyword : <b>{{ Request::get('keyword') }}</b>
                    </div>
                @endif

                @include('alert.success')

                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th width="5%">No</th>
                            <th>Nama</th>
                            <th>Username</th>
                            <th>Email</th>
                            <th>Level</th>
                            <th width="30%">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($user as $row)
                            <tr>
                                <td>{{ $loop->iteration + ($user->perpage() * ($user->currentPage() -1) ) }}</td>
                                <td>{{ $row->name }}</td>
                                <td>{{ $row->username }}</td>
                                <td>{{ $row->email }}</td>
                                <td>{{ $row->level }}</td>
                                <td>
                                    
                                    <form method="post" action="{{ route('user.destroy',[$row->id]) }}" onsubmit="return confirm('Apakah Anda Yakin Akan Menghapus Data Ini?');">
                                    @csrf
                                    {{ method_field('delete') }}
                                        <a class="btn btn-warning" href="{{ route('user.edit', [$row->id]) }}">Edit</a>
                                        <button type="submit" class="btn btn-danger">Delete</button>
                                        <a class="btn btn-info" href="{{ route('user.show',[$row->id]) }}">Detail</a>
                                    </form>
                                </td>
                            </tr>                            
                        @endforeach
                    </tbody>
                </table>
                {{ $user->appends(Request::all())->links() }}
                <!-- <nav aria-label="Page navigation example">
                    <ul class="pagination">
                        {{ $user->links() }}
                    </ul>   
                </nav> -->
            </div>
          </div>
        </div>
    </div>
@endsection