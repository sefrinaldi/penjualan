@extends('layouts.template')

@section('title')
    Edit Data Pegawai
@endsection

@section('content')
<div class="container">
  <div class="row">
    <div class="col-md-12">
      <div class="panel panel-default card">
        <div class="panel-heading">
          @include('alert.error')                           
        </div>

        <div class="panel-body">
          <form class="form-horizontal" method="post" action="{{ route('pegawai.update', [$pegawai->username]) }}">
          {{ csrf_field() }}
          {{ method_field('PUT') }}
            <div class="panel-body post-body">
              <div class="form-group ">
                <label for="username" class="col-sm-2 control-label">Username</label>
                <div class="col-sm-10">
                  <input type="text" class="form-control" id="username" disabled value="{{ $pegawai->username }}"></input>
                </div>
              </div>

              <div class="form-group">
                <label for="password" class="col-sm-2">Password</label>
                <div class="col-sm-10">
                  <input type="password" class="form-control" id="password" name="password" value=""></input>
                </div>
              </div>

              <div class="form-group">
                <label for="nama_pegawai" class="col-sm-2 control-label">Nama Pegawai</label>
                <div class="col-sm-10">
                  <input type="text" class="form-control" id="nama_pegawai" name="nama_pegawai" value="{{ $pegawai->nama_pegawai }}"></input>
                </div>
              </div>

              <div class="form-group">
                <label for="jk" class="col-sm-2 control-label">Jenis Kelamin</label>
                <div class="col-sm-10">
                  <select class="form-control" id="jk" name="jk">
                    <option value="PRIA" @if($pegawai->jk == "PRIA") selected @endif >Pria</option>
                    <option value="WANITA" @if($pegawai->jk == "WANITA") selected @endif>Wanita</option>
                  </select>
                </div>
              </div>

              <div class="form-group">
                <label for="alamat" class="col-sm-2 control-label">Alamat</label>
                <div class="col-sm-10">
                  <textarea class="form-control" id="alamat" name="alamat">{{ $pegawai->alamat }}</textarea>
                </div>
              </div>

              <div class="form-group">
                <label for="is_aktif" class="col-sm-10 control-label">Status</label>
                <div class="col-sm-10">
                  <select name="is_aktif" id="is_aktif" class="form-control">
                    <option value="1" @if($pegawai->is_aktif == 1) selected @endif >Aktif</option>
                    <option value="0" @if($pegawai->is_aktif == 0) selected @endif >Tidak Aktif</option>
                  </select>
                </div>
              </div>
            </div>

            <div class="panel-footer col-sm-10">    
              <a class="btn btn-info float-right" href="{{ route('pegawai.index') }}">Back</a>
              <button type="submit" name="tombol" class="btn btn-info float-right">Update</button>          
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection