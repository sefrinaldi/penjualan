@extends('layouts.template')

@section('title')
    Report Penjualan
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
          <div class="panel panel-default">
            <div class="panel-heading with-border">   
                <a href="{{ route('cetak_pdf') }}" class="btn btn-success" target="_blank">PDF</a>  
                <a href="{{ route('cetak_excel') }}" class="btn btn-danger" target="_blank">EXCEL</a>                   
            </div>
            <div class="panel panel-body">
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th width="5%">No</th>
                            <th>Nama Produk</th>
                            <th>Jumlah</th>
                            <th>Harga</th>
                            <th>Tanggal</th>
                            <th>Agen</th>                            
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($penjualan as $row)
                            <tr>
                                <td>{{ $loop->iteration + ($penjualan->perPage() * ($penjualan->currentPage() - 1)) }}</td>
                                <td>{{ $row->produk->nama_produk }}</td>
                                <td>{{ $row->jumlah }}</td>
                                <td>@rupiah($row->harga)</td>
                                <td>{{ $row->transaksi->tgl_penjualan }}</td>
                                <td>{{ $row->transaksi->agen->nama_toko }}</td>                                
                            </tr>
                        @endforeach
                    </tbody>
                </table>  
                {{ $penjualan->links() }}                                       
            </div>
          </div>
        </div>
    </div>
@endsection