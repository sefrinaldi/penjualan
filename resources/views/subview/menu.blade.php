<nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
          <li class="nav-item has-treeview">
            <a href="#" class="nav-link ">
              <i class="nav-icon fas fa-tachometer-alt"></i>
              <p>
                Master
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>

            <ul class="nav nav-treeview">
              @if(Auth::user()->level == 'admin')
              <li class="nav-item">
                <a href="{{ route('user.index') }}" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>User</p>
                </a>
              </li>
              @endif
              <li class="nav-item">
                <a href="{{ route('supplier.index') }}" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Supplier</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{ route('pegawai.index') }}" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Pegawai</p>
                </a>
              </li>              
              <li class="nav-item">
                <a href="{{ route('kategori.index') }}" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Kategori</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{ route('produk.index') }}" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Produk</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{ route('agen') }}" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Agen</p>
                </a>
              </li>
            </ul>
          </li>

          <li class="nav-item">
            <a href="{{ route('corona') }}" class="nav-link">
              <i class="nav-icon fas fa-radiation"></i>
              <p>
                Info Corona               
              </p>
            </a>
          </li>

          <li class="nav-item">
            <a href="{{ route('transaksi_masuk.index') }}" class="nav-link">
              <i class="nav-icon fas fa-th"></i>
              <p>
                Transaksi Masuk                
              </p>
            </a>
          </li>

          <li class="nav-item">
            <a href="{{ route('report_penjualan') }}" class="nav-link">
              <i class="nav-icon fas fa-th"></i>
              <p>
                Report Penjualan                
              </p>
            </a>
          </li>
        </ul>
      </nav>