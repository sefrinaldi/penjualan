@extends('layouts.template')

@section('title')
    Tambah Data Transaksi Masuk
@endsection

@section('content')
<div class="container">
  <div class="row">
    <div class="col-md-12">
      <div class="panel panel-default card">
        <div class="panel-heading">
          @include('alert.error')                           
        </div>

        <div class="panel-body">
          <form class="form-horizontal" method="post" action="{{ route('transaksi_masuk.store') }}">
          {{ csrf_field() }}
            <div class="panel-body post-body">
              <div class="form-group ">
                <label for="kd_produk" class="col-sm-2 control-label">Produk</label>
                <div class="col-sm-10">                  
                  <select class="form-control" id="kd_produk" name="kd_produk">
                    @foreach($produk as $row)
                      <option value="{{ $row->kd_produk }}">{{ $row->nama_produk }}</option>
                    @endforeach
                  </select>
                </div>
              </div>

              <div class="form-group ">
                <label for="kd_supplier" class="col-sm-2 control-label">Supplier</label>
                <div class="col-sm-10">                  
                  <select class="form-control" id="kd_supplier" name="kd_supplier">
                    @foreach($supplier as $row)
                      <option value="{{ $row->kd_supplier }}">{{ $row->nama_supplier }}</option>
                    @endforeach
                  </select>
                </div>
              </div>

              <div class="form-group ">
                <label for="tgl_transaksi" class="col-sm-2 control-label">Tanggal Transaksi</label>
                <div class="col-sm-10">                  
                  <input type="text" id="tgl_transaksi" name="tgl_transaksi" class="form-control datepicker" value="{{ old('tgl_transaksi') }}" readonly />
                </div>
              </div>

              <div class="form-group ">
                <label for="jumlah" class="col-sm-2 control-label">Jumlah</label>
                <div class="col-sm-10">                  
                  <input type="number" id="jumlah" name="jumlah" class="form-control" value="{{ old('jumlah') }}"></input>
                </div>
              </div>

              <div class="form-group">
                <label for="harga" class="col-sm-2 control-label">Harga</label>
                <div class="col-sm-10">
                  <input type="number" id="harga" name="harga" class="form-control" value="{{ old('harga') }}"></input>
                </div>
              </div>                            

            <div class="panel-footer col-sm-10">    
              <a class="btn btn-info float-right" href="{{ route('transaksi_masuk.index') }}">Back</a>
              <button type="submit" name="tombol" class="btn btn-info float-right">Save</button>          
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection