@extends('layouts.template')

@section('title')
    Tambah Data Kategori
@endsection

@section('content')
<div class="container">
  <div class="row">
    <div class="col-md-12">
      <div class="panel panel-default card">
        <div class="panel-heading">
          @include('alert.error')                           
        </div>

        <div class="panel-body">
          <form class="form-horizontal" method="post" action="{{ route('kategori.store') }}" enctype="multipart/form-data">
          {{ csrf_field() }}
            <div class="panel-body post-body">
              <div class="form-group ">
                <label for="kategori" class="col-sm-2 control-label">Kategori</label>
                <div class="col-sm-10">
                  <input type="text" class="form-control" id="kategori" name="kategori" value="{{ old('kategori') }}"></input>
                </div>
              </div>

              <div class="form-group">
                <label for="gambar_kategori" class="col-sm-2 control-label">Gambar Kategori</label>
                <div class="col-sm-10">
                  <input type="file" id="gambar_kategori" name="gambar_kategori" class="form-control"></input>
                </div>
              </div>                            

            <div class="panel-footer col-sm-10">    
              <a class="btn btn-info float-right" href="{{ route('kategori.index') }}">Back</a>
              <button type="submit" name="tombol" class="btn btn-info float-right">Save</button>          
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection