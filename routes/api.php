<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\AgenController;
use App\Http\Controllers\Api\PegawaiController;
use App\Http\Controllers\Api\KategoriController;
use App\Http\Controllers\Api\ProdukController;
use App\Http\Controllers\Api\TransaksiController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::apiResource('agen', AgenController::class);
Route::get('search_agen', [AgenController::class, 'search']);
Route::post('login_pegawai', [PegawaiController::class, 'login_pegawai']);
Route::get('get_kategori', [KategoriController::class, 'get_all']);
Route::post('get_produk', [ProdukController::class, 'get_produk_kategori']);
Route::post('add_cart', [TransaksiController::class, 'add_cart']);
Route::post('get_cart', [TransaksiController::class, 'get_cart']);
Route::post('delete_item_cart', [TransaksiController::class, 'delete_item_cart']);
Route::post('delete_cart', [TransaksiController::class, 'delete_cart']);
Route::post('checkout', [TransaksiController::class, 'checkout']);
Route::post('get_transaksi', [TransaksiController::class, 'get_transaksi']);
Route::post('get_detail_transaksi', [TransaksiController::class, 'get_detail_transaksi']);