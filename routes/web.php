<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\SupplierController;
use App\Http\Controllers\PegawaiController;
use App\Http\Controllers\KategoriController;
use App\Http\Controllers\ProdukController;
use App\Http\Controllers\TransaksiMasukController;
use App\Http\Controllers\AgenController;
use App\Http\Controllers\ReportPenjualanController;
use App\Http\Controllers\InfoCoronaController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('login');
});

Auth::routes();

Route::match(["get","post"],"/register", function(){
    return redirect('login');
})->name("register");

Route::group(['middleware' => ['auth']], function(){
Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::resource('user','App\Http\Controllers\UserController');
Route::resource('supplier', SupplierController::class)->except(['show']);
Route::resource('pegawai', PegawaiController::class)->except('show');
Route::resource('kategori', KategoriController::class)->except('show');
Route::resource('produk', ProdukController::class)->except('show');
Route::resource('transaksi_masuk', TransaksiMasukController::class)->only(['index', 'create', 'store', 'destroy']);
Route::get('agen', [AgenController::class, 'index'])->name('agen');
Route::get('report_penjualan', [ReportPenjualanController::class, 'index'])->name('report_penjualan');
Route::get('cetak_pdf', [ReportPenjualanController::class, 'cetak_pdf'])->name('cetak_pdf');
Route::get('cetak_excel', [ReportPenjualanController::class, 'cetak_excel'])->name('cetak_excel');
Route::get('corona', [InfoCoronaController::class, 'index'])->name('corona');


});

